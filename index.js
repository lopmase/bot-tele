const { google } = require('googleapis');
const fetch = require('node-fetch');
const TelegramBot = require('node-telegram-bot-api');
const Holidays = require('date-holidays')
const fs = require('fs');

// Thông tin xác thực cho Google Sheets API
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
const credentials = JSON.parse(fs.readFileSync('./micro-liberty-279210-fb8db3fcefc6.json'));
const hd = new Holidays('VN');
const today = new Date();
console.log(today);return;
console.log(hd.isHoliday(new Date(now())));return;

const auth = new google.auth.GoogleAuth({
    scopes: SCOPES,
    credentials: credentials,
});

const sheets = google.sheets({ version: 'v4', auth });
// Lưu trữ trạng thái người dùng
let userState = {};
let authorizedUserId = null;

// Token của bot và ID của group
const TOKEN = '7363765877:AAFanOYfuPsBSdqEfLYqM9KbZcf_vKuP0kg';
const GROUP_ID = '-1002232912115';  // Hoặc username của group (bắt đầu bằng @)

// Khởi tạo bot
const bot = new TelegramBot(TOKEN, { polling: true });

bot.onText(/\/getInf/, (msg) => {
    const userId = msg.from.id;
    if (!authorizedUserId) {
        authorizedUserId = userId;
        bot.sendMessage(chatId, 'Chào mừng! Vui lòng nhập /getData để sử dụng bot tiếp tục');
        userState[userId] = { step: 'waiting_for_sheet_id', chatId: chatId };
    } else {
        bot.sendMessage(chatId, 'Bot hiện chỉ lắng nghe tin nhắn từ một người dùng.');
    }
});
bot.onText(/\/stop/, (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;

    if (userId === authorizedUserId) {
        bot.sendMessage(chatId, 'Bot đã dừng hoạt động.');

        // Reset trạng thái và các biến liên quan
        delete userState[userId];
        authorizedUserId = null;
        // Dừng polling
        bot.stopPolling();
    } else {
        bot.sendMessage(chatId, 'Bạn không có quyền dừng bot.');
    }
});
bot.onText(/\/getData/, (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;

    if (!authorizedUserId) {
        authorizedUserId = userId;
        bot.sendMessage(chatId, 'Chào mừng! Vui lòng nhập ID của Google Sheets mà bạn muốn lấy dữ liệu:');
        userState[userId] = { step: 'waiting_for_sheet_id', chatId: chatId };
    } else {
        bot.sendMessage(chatId, 'Bot hiện chỉ lắng nghe tin nhắn từ một người dùng.');
    }

    bot.sendMessage(chatId, 'Nếu muốn lấy được dữ liệu vui lòng share quyền truy cập google sheet cho email : test-gg-sheet@micro-liberty-279210.iam.gserviceaccount.com');

    bot.sendMessage(chatId, 'Vui lòng nhập ID của Google Sheets mà bạn muốn lấy dữ liệu:');
    bot.sendMessage(chatId, 'ví dụ : https://docs.google.com/spreadsheets/d/id');
});
// Lắng nghe tin nhắn để lấy ID của group
bot.on('message', async (msg) => {
    const chatId = msg.chat.id;
    const text = msg.text;
    const userId = msg.from.id;

    if (userId === authorizedUserId && userState[userId]) {
        const state = userState[userId];

        if (state.step === 'waiting_for_sheet_id') {
            state.sheetId = text;
            const isValidSheetId = await validateSheetId(text);
            if (isValidSheetId) {
                state.sheetId = text;
                state.step = 'waiting_for_range';
                bot.sendMessage(chatId, 'Vui lòng nhập tên sheet bạn muốn lấy dữ liệu:');
            } else {
                bot.sendMessage(chatId, 'ID Google Sheets không hợp lệ. Vui lòng nhập lại:');
            }
        } else if (state.step === 'waiting_for_range') {
            state.range = text;

            const isValidSheetName = await validateSheetName(state.sheetId, state.range);
            if (isValidSheetName) {
                // Lấy dữ liệu từ Google Sheets và gửi lại cho người dùng
                try {
                    getData(state.sheetId, state.range, chatId);
                } catch (error) {
                    console.error('Lỗi:', error);
                    await sendMessageToGroup(chatId, 'Đã xảy ra lỗi khi lấy dữ liệu.');
                }

                // Reset trạng thái người dùng
                delete userState[userId];
                authorizedUserId = null;
            } else {
                bot.sendMessage(chatId, 'Tên sheet không tồn tại. Vui lòng nhập lại:');
            }
        }
    }
});

// Hàm kiểm tra ID Google Sheets
async function validateSheetId(spreadsheetId) {
    try {
        await sheets.spreadsheets.get({
            spreadsheetId: spreadsheetId,
        });
        return true;
    } catch (error) {
        return false;
    }
}

async function validateSheetName(spreadsheetId, sheetName) {
    try {
        const spreadsheetInfo = await sheets.spreadsheets.get({
            spreadsheetId: spreadsheetId,
        });

        const sheet = spreadsheetInfo.data.sheets.find(sheet => sheet.properties.title === sheetName);

        if (!sheet) {
            return false;
        }

        return true;
    } catch (error) {
        return false;
    }
}


// Hàm lấy dữ liệu từ Google Sheets
async function getDataFromSheet(spreadsheetId, rangeSheet) {
    try {
        const res = await sheets.spreadsheets.values.get({
            spreadsheetId: spreadsheetId,
            range: rangeSheet,  // Thay đổi phạm vi tùy theo sheet của bạn
            majorDimension: 'ROWS'
        });
    return res.data.values;

    } catch (error) {
        console.error('Lỗi:', error);
        await sendMessageToGroup(chatId, 'Đã xảy ra lỗi khi lấy dữ liệu.');
    }
}

// Hàm gửi tin nhắn đến group
async function sendMessageToGroup(message, chatId) {
    await bot.sendMessage(chatId, message);
}

async function getData(spreadsheetId, sheet, chatId) {
    try {
        let rangeStart = sheet + '!A1:Z7';
        const dataStart = await getDataFromSheet(spreadsheetId, rangeStart);
        let message = "Dữ liệu từ Google Sheets:\n";
        let messageStart = "";
        let messageStock = "\n";
        let spaceString = " -";

        let listIndexGetInfo = [
            7,
            8,
            9,
            10,
            19
        ]
        dataStart.forEach(row => {
            messageStart += row[1] + ' : ' + row[2] + '\n';
        });
        let rangeStock = sheet + '!A8:Z1000';
        const dataStock = await getDataFromSheet(spreadsheetId, rangeStock);
        dataStock.forEach(rowStock => {
            if (rowStock[6] == "Keeping") {
                rowStock.forEach((data, index) => {
                    if (listIndexGetInfo.includes(index)) {
                        let stringData = (data ?? "") + spaceString;
                        switch (index) {
                            case 8:
                                messageStock += " KL : ";
                                break;
                            case 9:
                                messageStock += " Giá mua : ";
                                break;
                            case 10:
                                messageStock += " Giá bán : ";
                                break;
                        }

                        if (index == 19) {
                            messageStock += spaceString + " Lãi/ Lỗ : " + (rowStock[5] ?? 'Not input') + '\n';
                            return;
                        }

                        if (index == 10) {
                            stringData = (data != '' ? data : rowStock[11]);
                        }

                        messageStock += stringData;
                    }
                })
            }
        });

        message += messageStart + '\n' + messageStock
        await sendMessageToGroup(message, chatId);
        console.log("Đã gửi tin nhắn thành công!");
    } catch (error) {
        console.error('Lỗi:', error);
        await sendMessageToGroup(chatId, 'Đã xảy ra lỗi khi lấy dữ liệu.');
    }
}

